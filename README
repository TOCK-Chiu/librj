My target application is wget. You can try this command to verify my work:
$ env LD_PRELOAD=./libRJ.so wget 'http://www.example.com/'

The environment of development is:
 - Linux 4.4.5-1-ARCH on x86_64
 - glibc 2.23
 - Clang 3.7.1
 - libstdc++ shipped with GCC 5.3.0

My work intercepts these API calls:
 - creat
 - mkdir
 - rmdir
 - opendir
 - closedir
 - open
 - close
 - fopen
 - freopen
 - fclose
 - read
 - fread
 - write
 - fwrite
 - unlinkat
 - rename
 - remove
 - unlink
 - connect
 - socket
 - recv
 - send
 - SSL_write (OpenSSL support)
 - SSL_read (OpenSSL support)

My work also supports intercepting SSL calls. However, it currently only supports
two APIs, SSL_read and SSL_write, from OpenSSL. The arguments of each call to
these APIs are intercepted and outputted. E.g.:
[libRJ] fopen("/etc/wgetrc", "r") = 0xe8ed00

You can find that all messages start by "[libRJ] " so that a user can easily
tell the output of libRJ and the output of original application apart. The
following value is the returned value of the call. libRJ output more info for
some APIs. For example it output the target IP of the calls to connect like:
[libRJ] connect(3, 0x7fde3f5f8ad4 (8.8.8.8), 16) = 0

and, especially, I/O APIs:
[libRJ] write(3, 0x7ffd65a36070, 142) = 142
[libRJ]      0 | 47 45 54 20 2f 20 48 54 54 50 2f 31 2e 31 0d 0a | GET / HTTP/1.1
...

The data stored in the buffer passed to the APIs read, fread, write, fwrite,
SSL_read and SSL_write is intercepted and outputted by libRJ. Please note that
if the target of fwrite is stdout or stderr, libRJ will not generate any output.
That is because fprintf, which is used to output info by libRJ, may call fwrite.
If libRJ call fprintf for each fwrite call, it's likely resulting in infinite
recursion. For APIs that read data into memory, libRJ prints the data it really
read. However, For APIs that write data into other place from memory, libRJ
prints the whole buffer. That is because libRJ try to preserve all info.
