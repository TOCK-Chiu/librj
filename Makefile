
CXX      = g++
CXXFLAGS = -Wall -Wextra -O2 -std=c++14 -pedantic -flto
LDFLAGS  = -s
TARGET   = libRJ.so
SRCS     = $(wildcard *.cpp)
OBJS     = ${SRCS:%.cpp=%.o}

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -fPIC -shared -ldl -o $(TARGET)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -fPIC $< -c -o $@

.PHONY: clean

clean:
	$(RM) $(TARGET) *.o
