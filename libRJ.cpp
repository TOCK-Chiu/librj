#include <cstdio>
#include <cstdarg>
#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <dirent.h>
#include <fcntl.h>
#include <dlfcn.h>





////////////////////////////////////////////////////////////////////////////////
// Helper functions.
////////////////////////////////////////////////////////////////////////////////
namespace libRJ{

	void LogOneLineInfo(const char* Info, ...){

		va_list ArgList;

		va_start(ArgList, Info);
		fprintf(stderr, "[libRJ] ");
		vfprintf(stderr, Info, ArgList);
		fprintf(stderr, "\n");
		va_end(ArgList);

		}

	void PrintBuffer(const void* Buffer, size_t Size){

		const uint8_t* ByteBuffer = reinterpret_cast<const uint8_t*>(Buffer);
		size_t Index = 0;

		while( Size > 0 ){

			size_t Edge = std::min<size_t>(Size, 16);
			size_t LogPos = 0;
			size_t IntraIndex = 0;
			char LogBuffer[256] = {0};
			char StrBuffer[32] = {0};

			// Gutter: Byte pos region.
			LogPos = sprintf(LogBuffer, "%6zu |", Index);

			// Hex region.
			for(IntraIndex = 0; IntraIndex < Edge; IntraIndex++){

				LogPos += sprintf(&LogBuffer[LogPos], " %02x", ByteBuffer[Index]);

				if( ByteBuffer[Index] < 32 || ByteBuffer[Index] >= 127 )
					StrBuffer[IntraIndex] = ' ';
				else
					StrBuffer[IntraIndex] = ByteBuffer[Index];

				Index++;

				}

			Size -= Edge;
			libRJ::LogOneLineInfo("%-56s | %s", LogBuffer, StrBuffer);

			}

		}

	}





////////////////////////////////////////////////////////////////////////////////
// Wrapper functions
////////////////////////////////////////////////////////////////////////////////
extern "C" int creat(const char* Path, mode_t Mode){

	auto Ptr = reinterpret_cast<decltype(&creat)>( dlsym(RTLD_NEXT, "creat") );
	int Result = Ptr(Path, Mode);

	libRJ::LogOneLineInfo("creat(\"%s\", 0%o) = %d", Path, Mode, Result);

	return Result;

	}



extern "C" int mkdir(const char *Path, mode_t Mode) __THROW {

	auto Ptr = reinterpret_cast<decltype(&mkdir)>( dlsym(RTLD_NEXT, "mkdir") );
	int Result = Ptr(Path, Mode);

	libRJ::LogOneLineInfo("mkdir(\"%s\", 0%o) = %d", Path, Mode, Result);

	return Result;

	}



extern "C" int rmdir(const char *Path){

	auto Ptr = reinterpret_cast<decltype(&rmdir)>( dlsym(RTLD_NEXT, "rmdir") );
	int Result = Ptr(Path);

	libRJ::LogOneLineInfo("rmdir(\"%s\") = %d", Path, Result);

	return Result;

	}



extern "C" DIR* opendir(const char* Name){

	auto Ptr = reinterpret_cast<decltype(&opendir)>( dlsym(RTLD_NEXT, "opendir") );
	DIR* Result = Ptr(Name);

	libRJ::LogOneLineInfo("opendir(\"%s\") = %p", Name, Result);

	return Result;

	}



extern "C" int closedir(DIR* Dir){

	auto Ptr = reinterpret_cast<decltype(&closedir)>( dlsym(RTLD_NEXT, "closedir") );
	int Result = Ptr(Dir);

	libRJ::LogOneLineInfo("closedir(%p) = %d", Dir, Result);

	return Result;

	}



extern "C" int open(const char* Path, int Flags, ...){

	auto Ptr = reinterpret_cast<decltype(&open)>( dlsym(RTLD_NEXT, "open") );
	int Result;

	// Mode will be ignore in these case.
	if( ! ( Flags & (O_CREAT | O_TMPFILE) ) ){

		Result = Ptr(Path, Flags);
		libRJ::LogOneLineInfo("open(\"%s\", 0%o) = %d", Path, Flags, Result);

		return Result;

		}

	mode_t Mode;
	va_list List;

	va_start(List, Flags);
	Mode = va_arg(List, mode_t);
	Result = Ptr(Path, Flags, Mode);
	libRJ::LogOneLineInfo("open(\"%s\", 0%o, 0%o) = %d", Path, Flags, Mode, Result);

	return Result;

	}



extern "C" int close(int FD){

	auto Ptr = reinterpret_cast<decltype(&close)>( dlsym(RTLD_NEXT, "close") );
	int Result = Ptr(FD);

	libRJ::LogOneLineInfo("close(%d) = %d", FD, Result);

	return Result;

	}



extern "C" FILE* fopen(const char* Filename, const char* Mode){

	auto Ptr = reinterpret_cast<decltype(&fopen)>( dlsym(RTLD_NEXT, "fopen") );
	FILE* Result = Ptr(Filename, Mode);

	libRJ::LogOneLineInfo("fopen(\"%s\", \"%s\") = %p", Filename, Mode, Result);

	return Result;

	}



extern "C" FILE* freopen(const char* Path, const char* Mode, FILE *Stream){

	auto Ptr = reinterpret_cast<decltype(&freopen)>( dlsym(RTLD_NEXT, "freopen") );
	FILE* Result = Ptr(Path, Mode, Stream);

	libRJ::LogOneLineInfo("freopen(\"%s\", \"%s\", %p) = %p", Path, Mode, Stream, Result);

	return Result;

	}



extern "C" int fclose(FILE* Stream){

	auto Ptr = reinterpret_cast<decltype(&fclose)>( dlsym(RTLD_NEXT, "fclose") );
	int Result = Ptr(Stream);

	libRJ::LogOneLineInfo("fclose(%p) = %d", Stream, Result);

	return Result;

	}



extern "C" ssize_t read(int FD, void* Buffer, size_t Count){

	auto Ptr = reinterpret_cast<decltype(&read)>( dlsym(RTLD_NEXT, "read") );
	ssize_t Result = Ptr(FD, Buffer, Count);

	libRJ::LogOneLineInfo("read(%d, %p, %zu) = %zd", FD, Buffer, Count, Result);

	if( Result > 0 )
		libRJ::PrintBuffer(Buffer, Result);

	return Result;

	}



extern "C" size_t fread(void* Buffer, size_t Size, size_t Count, FILE* Stream){

	auto Ptr = reinterpret_cast<decltype(&fread)>( dlsym(RTLD_NEXT, "fread") );
	size_t Result = Ptr(Buffer, Size, Count, Stream);

	libRJ::LogOneLineInfo("fread(%p, %zu, %zu, %p) = %zu", Buffer, Size, Count, Stream, Result);

	if( Result > 0 )
		libRJ::PrintBuffer(Buffer, Result * Count);

	return Result;

	}




extern "C" ssize_t write(int FD, void* Buffer, size_t Count){

	auto Ptr = reinterpret_cast<decltype(&write)>( dlsym(RTLD_NEXT, "write") );
	ssize_t Result = Ptr(FD, Buffer, Count);

	libRJ::LogOneLineInfo("write(%d, %p, %zu) = %zd", FD, Buffer, Count, Result);
	libRJ::PrintBuffer(Buffer, Count);

	return Result;

	}



extern "C" size_t fwrite(const void* Buffer, size_t Size, size_t Count, FILE* Stream){

	auto Ptr = reinterpret_cast<decltype(&fwrite)>( dlsym(RTLD_NEXT, "fwrite") );
	size_t Result = Ptr(Buffer, Size, Count, Stream);

	// Prevent infinite recursion if fprintf calls fwrite.
	if( Stream != stdout && Stream != stderr ){

		libRJ::LogOneLineInfo("fwrite(%p, %zu, %zu, %p) = %zu", Buffer, Size, Count, Stream, Result);
		libRJ::PrintBuffer(Buffer, Size * Count);

		}

	return Result;

	}



extern "C" int unlinkat(int DirFD, const char *Path, int Flags){

	auto Ptr = reinterpret_cast<decltype(&unlinkat)>( dlsym(RTLD_NEXT, "unlinkat") );
	int Result = Ptr(DirFD, Path, Flags);

	libRJ::LogOneLineInfo("unlinkat(%d, \"%s\", 0%o) = %d", DirFD, Path, Flags, Result);

	return Result;

	}



extern "C" int rename(const char* OldPath, const char* NewPath) __THROW {

	auto Ptr = reinterpret_cast<decltype(&rename)>( dlsym(RTLD_NEXT, "rename") );
	int Result = Ptr(OldPath, NewPath);

	libRJ::LogOneLineInfo("rename(\"%s\", \"%s\") = %d", OldPath, NewPath, Result);

	return Result;

	}



extern "C" int remove(const char* Path) __THROW {

	auto Ptr = reinterpret_cast<decltype(&remove)>( dlsym(RTLD_NEXT, "remove") );
	int Result = Ptr(Path);

	libRJ::LogOneLineInfo("remove(\"%s\") = %d", Path, Result);

	return Result;

	}



extern "C" int unlink(const char* Path){

	auto Ptr = reinterpret_cast<decltype(&unlink)>( dlsym(RTLD_NEXT, "unlink") );
	int Result = Ptr(Path);

	libRJ::LogOneLineInfo("unlink(\"%s\") = %d", Path, Result);

	return Result;

	}


extern "C" int connect(int SocketFD,
                       const struct sockaddr* Addr,
                       socklen_t AddrLen){

	auto Ptr = reinterpret_cast<decltype(&connect)>( dlsym(RTLD_NEXT, "connect") );
	const struct sockaddr_in* AddrIn = reinterpret_cast<decltype(AddrIn)>( Addr );
	constexpr size_t IP_STR_MAX_LENGTH = std::max(INET_ADDRSTRLEN, INET6_ADDRSTRLEN);
	char IpStr[ IP_STR_MAX_LENGTH ] = {0};
	int Result = Ptr(SocketFD, Addr, AddrLen);

	inet_ntop(AddrIn->sin_family,
	         &(AddrIn->sin_addr),
	         IpStr,
	         AddrLen);

	libRJ::LogOneLineInfo("connect(%d, %p (%s), %zu) = %d", SocketFD, Addr, IpStr, AddrLen, Result);

	return Result;

	}



extern "C" int socket(int Domain, int Type, int Protocol) __THROW {

	auto Ptr = reinterpret_cast<decltype(&socket)>( dlsym(RTLD_NEXT, "socket") );
	int Result = Ptr(Domain, Type, Protocol);

	libRJ::LogOneLineInfo("socket(%d, %d, %d) = %d", Domain, Type, Protocol, Result);

	return Result;

	}



extern "C" ssize_t recv(int SocketFD, void* Buffer, size_t Len, int Flags){

	auto Ptr = reinterpret_cast<decltype(&recv)>( dlsym(RTLD_NEXT, "recv") );
	ssize_t Result = Ptr(SocketFD, Buffer, Len, Flags);

	libRJ::LogOneLineInfo("recv(%d, %p, %zu, %0o) = %zd", SocketFD, Buffer, Len, Flags, Result);

	if( Result > 0 )
		libRJ::PrintBuffer(Buffer, Result);

	return Result;

	}



extern "C" ssize_t send(int SocketFD, const void* Buffer, size_t Len, int Flags){

	auto Ptr = reinterpret_cast<decltype(&send)>( dlsym(RTLD_NEXT, "send") );
	ssize_t Result = Ptr(SocketFD, Buffer, Len, Flags);

	libRJ::LogOneLineInfo("send(%d, %p, %zu, %0o) = %zd", SocketFD, Buffer, Len, Flags, Result);
	libRJ::PrintBuffer(Buffer, Len);

	return Result;

	}



extern "C" int SSL_write(SSL *Ssl, const void* Buffer, int Size){

	auto Ptr = reinterpret_cast<decltype(&SSL_write)>( dlsym(RTLD_NEXT, "SSL_write") );
	int Result = Ptr(Ssl, Buffer, Size);

	libRJ::LogOneLineInfo("SSL_write(%p, %p, %d) = %d", Ssl, Buffer, Size, Result);

	// Well, I dunno why they don't use size_t.
	if( Size > 0 )
		libRJ::PrintBuffer(Buffer, Size);

	return Result;

	}



extern "C" int SSL_read(SSL *Ssl, void* Buffer, int Size){

	auto Ptr = reinterpret_cast<decltype(&SSL_read)>( dlsym(RTLD_NEXT, "SSL_read") );
	int Result = Ptr(Ssl, Buffer, Size);

	libRJ::LogOneLineInfo("SSL_read(%p, %p, %d) = %d", Ssl, Buffer, Size, Result);

	if( Result > 0 )
		libRJ::PrintBuffer(Buffer, Result);

	return Result;

	}
